// // if - else
var nama = "junaedi";
var peran = "Werewolf";

if (nama == "" && peran == "") {
  if (nama != "" && peran == "") {
    console.log("Hallo " + nama + ", Pilih peranmu untuk memulai game");
  }
} else if (nama != "" && peran != "") {
  console.log("Selamat datang di Dunia werewolf, " + nama);
  console.log(
    "Hallo " +
      peran +
      " " +
      nama +
      ", kamu dapat melihat siapa saja yang menjadi werewolf!"
  );
} else if (nama != "" && peran != "") {
  console.log("Selamat datang di Dunia werewolf, " + nama);
  console.log(
    "Hallo " +
      peran +
      " " +
      nama +
      ", akan membantu melindungi temanmu dari serangan werewolf."
  );
} else if (nama != "" && peran != "") {
  console.log("Selamat datang di Dunia werewolf, " + nama);
  console.log(
    "Hallo " + peran + " " + nama + ", Kamu akan memakan mangsa setiap malam!"
  );
} else {
  console.log("Nama harus di isi !");
}

// switch case
// Maka hasil yang akan tampil di console adalah: '21 Januari 1945';
var hari = 21;
var bulan = 1;
var tahun = 1945;

switch (bulan) {
  case 1:
    console.log(hari + " Januari " + tahun);
    break;
  case 2:
    console.log(hari + " Febuari " + tahun);
    break;
  case 3:
    console.log(hari + " Maret " + tahun);
    break;
  case 4:
    console.log(hari + " April " + tahun);
    break;
  case 5:
    console.log(hari + " Mei " + tahun);
    break;
  case 6:
    console.log(hari + " Juni " + tahun);
    break;
  case 7:
    console.log(hari + " Juli " + tahun);
    break;
  case 8:
    console.log(hari + " Agustus " + tahun);
    break;
  case 9:
    console.log(hari + " September " + tahun);
    break;
  case 10:
    console.log(hari + " Oktober " + tahun);
    break;
  case 11:
    console.log(hari + " November " + tahun);
    break;
  case 12:
    console.log(hari + " Desember " + tahun);
    break;

  default:
    console.log("Bulan tidak valid");
    break;
}
