// Variable
var namaInstruktur = "Wahyu Prasetya";
var namaSiswa = "Regi";

// Tipe Data
// String
var alamat = "Bandung";
var tanggalLahir ='01-09-1999';
var hobi = `Sepeda, 
lari`;

// Backtick
var ttl = `${alamat}, ${tanggalLahir}`;
console.log(ttl)

// Number 
var umur = 28 
var umurSiswa = 15 
console.log("berapa selisih umur siswa :", umur - umurSiswa);

// Boolean
var isStart = true ;
var isFinish = false ;
console.log(isStart);


// Operator
// operator assigment (=)
namaSiswa = "andrian saputra";
console.log(namaSiswa);

// operator aritmatika (+, -, *, /, %)
console.log("123 + 3 = ", 123 + 3);
console.log("2 * 3 = ", 2*3);
console.log("12 / 2 = ", 70 / 2);
console.log("Modulus 17 % 4 = ", 17 % 4);
console.log("Modulus 18 % 4 = ", 18%4);
console.log("Modulus 19 % 4 = ", 19%4);
console.log("Modulus 20 % 4 = ", 20%4);
console.log("Ganjil : sisanya = ", 19%2);
console.log("Genap : sisanya = ", 36%2);

// Operator perbandingan
// operator ==
var angka = 18;
console.log(angka == 18);
console.log(angka == 19);

// operator !=
console.log(angka != 20);
console.log(angka != 18)

// operator === strict equal 
console.log(angka === "18");
console.log(angka === 18);

var angka2 = 20

// Operator < > >= <=
console.log(angka2 >= angka)


// Operator Kondisional 
// || ATAU
// && Dan 

console.log( true || true )
console.log(true || false )
console.log(false || true)

console.log(angka ==  18 && angka%2 == 0)