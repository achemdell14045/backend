// Control flow / percabangan / Kondisional
// Review : Boolean, Operator Perbandingan, Operator Kondisional

// Contoh 1 menjalankan kode jika premis bernilai "true"
if (true) {
  console.log("Jalankan kode");
}

// Contoh 2 menjalankan kode jika premis bernilai "false"
if (false) {
  console.log("Kode tidak di jalankan");
}

// Contoh 3 Premis dengan nilai perbandingan suatu nilai
var mood = "happy";

if (mood == "happy") {
  console.log("hari ini aku sangat bahagia !");
}

// --------------------------------------------------------------------------------//

// Contoh 4 percabangan sederhana
var minimarketStatus = "open";

if (minimarketStatus == "close") {
  console.log("Saya akan membeli roko");
} else {
  console.log("minimarket tutup");
}

// contoh 5 percabangan dengan kondisi
var warung = "open";
var dikitLagiBuka = 5;

if (warung == "close") {
  console.log("saya akan membeli rokok");
} else if (dikitLagiBuka <= 5) {
  console.log("saya akan menunggu sebentar");
} else {
  console.log("saya akan pulang kembali");
}

// Contoh 6 Kondisional bersarang
var warungStatus = "open";
var rokok = "habis";
var korek = "habis";

if (warungStatus == "open") {
  console.log("saya akan membeli roko dan korek");
  if (rokok == "habis" || korek == "habis") {
    console.log(
      "roko nya kosong korek nya juga habis , Yahhh yaudah deh pulang aja"
    );
  } else if (rokok == "habis") {
    console.log("yahh roko nya habis , yaudah aku mau beli korek aja");
  } else if (korek == "habis") {
    console.log("yah korek nya juga habis");
  }
} else {
  console.log("yaahhh tutup pulang lagi ahh..");
}

// -----------------------------------------------------------------------------------------------------//

// Kondisional dengan switch case
// Cara lain untuk melakukan pengecekan kondisi atau conditional adalah dengan switch case.
// Penggunaan switch case mirip seperti kita menyalakan tombol switch pada remote. Jika kondisi tombol yang dipijit adalah tombol dengan nomer tertentu maka akan menjalankan prorgram tertentu.

// Contoh 7 kondisi dengan switch case
var pencetTombol = 1;

switch (pencetTombol) {
  case 1: {
    console.log("Matikan TV !");
    break;
  }
  case 2: {
    console.log("turunkan volume TV !");
    break;
  }
  case 3: {
    console.log("Tingkatkan volume TV !");
    break;
  }
  case 4: {
    console.log("Tidak terjadi apa apa");
    break;
  }
}
