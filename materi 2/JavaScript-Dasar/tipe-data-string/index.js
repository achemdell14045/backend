// Tipe Data String

// String adalah tipe data yang berisi karakter-karakter dibungkus dalam tanda petik ("" atau '' ). 
// Karakter-karakter pada suatu string dapat diakses dengan menggunakan indeks atau posisi karakter berada. Indeks pada string selalu mulai dari 0.
var sentece = "JavaScript";
console.log(sentece[0]); // J
console.log(sentece[2]); // v

// String Properties

// mengembalikan panjang atau jumlah karakter pada suatu string.
var word = "JavaScript is awesome";
console.log(word.length) // 21

// String Methods

// .charAt([index])
console.log(`i am a string `.charAt(3)); // 'm'

// .concat([string])
// Menggabungkan beberapa string dan mengembalikannya menjadi string baru.
var string1 = 'good';
var string2 = 'luck';
console.log(string1.concat(string2)); // goodluck


// .indexOf([string/karakter])
// Mengembalikan indeks dari string/karakter yang dicari, yang pertama kali ditemukan, atau -1 apabila tidak ditemukan.
var text = 'dung dung ces!';
console.log(text.indexOf('dung')); // 0
console.log(text.indexOf('u')); // 1
console.log(text.indexOf('jreng')); // -1

// .substring([index awal], [index akhir(opsional)])

// Mengembalikan potongan string mulai dari indeks pada parameter pertama (indeks awal) sampai dengan indeks pada parameter kedua (indeks akhir).
// Bila parameter kedua tidak ditentukan, maka secara otomatis berakhir pada karakter terakhir. Karakter pada indeks yang ditentukan pada parameter kedua tidak diikutkan sebagai output.
var car1 = `zelda motor`;
var car2 = car1.substr(6);
console.log(car2); //motor


// .substr([index awal], [jumlah karakter yang diambil(opsional)])

// Mendapatkan potongan string mulai dari indeks pada parameter pertama (indeks awal) dengan jumlah indeks pada parameter kedua (jumlah karakter).
// Bila parameter kedua tidak ditentukan, maka secara otomatis berakhir pada karakter terakhir. Karakter pada indeks yang ditentukan pada parameter kedua tidak diikutkan sebagai output.
var motor1 = `zelda motor`;
var motor2 = motor1.substr(2, 2);
console.log(motor2); // ld


// .topUpperCase()

// Mengembalikan string baru dengan semua karakter yang diubah menjadi huruf kapital.
var letter = `This Letter Is For You`;
var upper = letter.toUpperCase();
console.log(upper); // THIS LETTER IS FOR YOU


// .toLowerCase()

// Mengembalikan string baru dengan semua karakter yang diubah menjadi huruf kecil
var letter = `This Letter Is For You`;
var lower = letter.toLocaleLowerCase();
console.log(lower); // this letter is for you 


// .trim()

// Mengembalikan string baru yang sudah dihapus karakter whitespace (" ") pada awal dan akhir string tersebut.
var username = ' administrator ';
var newUsername = username.trim();
console.log(username); // 'adminstrator'




//---------------------------------------------------------------------------------------------------------------------------------------------------//

// Mengubah tipe data dari dan ke String
// Di Javascript terkadang kita ingin mengubah sebuah data string menjadi tipe data lain atau sebaliknya.
// Contoh diperoleh data angka tapi dalam tipe data String maka kita dapat mengubah string tersebut menjadi tipe data angka.


// String([angka/array])

// Fungsi global String() dapat dipanggil kapan saja pada program JavaScript dan akan mengembalikan data dalam tipe data String dari parameteryang di berikan.
var int = 12;
var real = 3.45;
var arr = [6, 7, 8];

var strInt = String(int);
var strReal = String(real);
var strArr = String(arr);

console.log(strInt);
console.log(strReal);
console.log(strArr);


// .toString()

// Mengonversi tipe data lain menjadi string. Bila data tersebut adalah array, setiap nilai akan dituliskan dan dipisah dengan karakter koma.
var number = 21;
console.log(number.toString());

var array = [1, 2];
console.log(array.toString());


// Number([string])

// Fungsi global Number() mengonversi tipe data string menjadi angka. Data yang diberikan pada parameter harus berupa karakter angka saja, dengan titik (separator) bila angka adalah bilangan desimal.
// Bila parameter berisi karakter selain angka dan/atau titik, Number() akan mengembalikan NaN (Not a Number).
var number1 = Number("90"); // number1 = 90
var number2 = Number("1.23"); // number2 = 1.23
var number3 = Number("4, 5"); // number3 = Nan

console.log("number1 = ",number1);
console.log("number2 = ",number2);
console.log("number3 = ",number3);


// parseInt([String]) dan parseFloat([string])

// Fungsi global parseInt([String]) dan parseFloat([String]) mengembalikan angka dari string.
// Bila angka adalah bilangan desimal maka gunakan parseFloat(), bila tidak bilangan dibelakang koma akan diabaikan.
var int = '89';
var real = '56.7';
var strInt_1 = parseInt(int);
var strInt_2 = parseInt(real);
var strReal_1 = parseFloat(int);
var strReal_2 = parseFloat(int);

console.log("strInt_1 = ",strInt_1);
console.log("strInt_2 = ",strInt_2);
console.log("strFloat_1 = ",strFloat_1);
console.log("strInt_1 = ",strInt_1);
