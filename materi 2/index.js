// data instruktur
var instruktur = require('./lib/instruktur');
// data siswa 
var siswa = require('./lib/siswa/index');


var namaInstruktur = instruktur.nama;
var alamatInstruktur = instruktur.alamat;


console.log("nama instruktur: ", namaInstruktur);
console.log("alamat instruktur: ", alamatInstruktur);   

// menampilkan data siswa
var dataSiswa1 = siswa.siswa1;
var dataSiswa2 = siswa.siswa2;

console.log("data siswa 1 : ", dataSiswa1);
console.log("data siswa 2 : ", dataSiswa2);